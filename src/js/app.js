document.addEventListener("DOMContentLoaded", function() {
    var fixedScrollTimeout = null;
    var body = document.querySelector('body');
    var hero = document.querySelector('.header-main');
    var heroHeight = hero.offsetHeight;

    function calcHeroHeight() {
        heroHeight = hero.offsetHeight;
    }

    window.addEventListener('resize', function(){
        calcHeroHeight();
    });

    window.addEventListener('scroll', function(e){
        if (window.scrollY > heroHeight+100){
            return;
        }

        if (window.scrollY > heroHeight) {
            body.style.paddingTop = heroHeight+"px";
            body.classList.add("fixed");
        } else {
            body.classList.remove("fixed");
            body.style.paddingTop = 0;
        }
    });

    var modalClose = document.querySelector('.modal-close');
    var modal = document.querySelector('.modal');
    var modalBackdrop = document.querySelector('.modal-backdrop');
    var modalBody = document.querySelector('.modal-body');

    var speakers = document.querySelectorAll('.card-speaker');

    for (i = 0; i < speakers.length; ++i) {
        speakers[i].addEventListener('click', function(e) {

            window.scrollTo(0, 0);

            var bio = this.querySelector(".speaker-bio").innerHTML;

            modalBody.innerHTML = bio;

            modal.style.display = "block";
            modalBackdrop.style.display = "block";
        });
    }

    modalClose.addEventListener("click", function(e) {
        modal.style.display = "none";
        modalBackdrop.style.display = "none";
    });

    modalBackdrop.addEventListener("click", function(e) {
        modal.style.display = "none";
        modalBackdrop.style.display = "none";
    });

});
